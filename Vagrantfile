Vagrant.configure( "2" ) do | configure |
  ##################################################
  # Custom.                                        #
  ##################################################
  apache_or_nginx = "apache"
  # TODO: Acually set the timezone.
  timezone        = "America/New_York"
  ##################################################

  configure.ssh.keep_alive               = true
  configure.ssh.max_tries                = 250
  # INFO: The password for "artisan" is "laravelrocks". [:)].
  configure.ssh.username                 = "artisan"
  configure.vm.box                       = "ubuntu-12.04-server-i386-laravel"
  configure.vm.box_url                   = "vagrant/box/ubuntu-12.04-server-i386-laravel.box"
  configure.vm.graceful_halt_retry_count = 10
  configure.vm.guest                     = :linux
  configure.vm.hostname                  = "laravel-dev"

  configure.vm.network :forwarded_port, {
    :auto_correct => true,
    :guest        => 80,
    :host         => 5272,
  }
  configure.vm.network :forwarded_port, {
    :auto_correct => true,
    :guest        => 3306,
    :host         => 5273,
  }
  # configure.vm.network :private_network, {
  #   :ip => "33.52.72.1",
  # }

  configure.vm.provider :virtualbox do | machine |
    machine.customize [
      "modifyvm",
      :id,
      "--cpus",
      "1",
    ]
    machine.customize [
      "modifyvm",
      :id,
      "--memory",
      "384",
    ]
    machine.customize [
      "modifyvm",
      :id,
      "--vtxvpid",
      "off",
    ]
  end

  ###########################################################
  # Vagrant's Ansible provisioner was being insolent to me. #
  # I'm doing inline shell provisioning for now.            #
  # This is until further notice.                           #
  #                                                         #
  # TODO: Use the Ansible provisioner.                      #
  ###########################################################
  configure.vm.provision :shell do | shell |
    shell.inline = <<-SCRIPT
      /usr/local/bin/ansible-playbook \
        -c local \
        -D \
        -e "apache_or_nginx=#{apache_or_nginx} timezone=#{timezone}" \
        -f 10 \
        -i /vagrant/ansible/hosts \
        -M /usr/share/ansible \
        -U root \
        -u vagrant \
        -v \
        /vagrant/ansible/main.yml
    SCRIPT
  end

  # TODO: Figure out why NFS just won't work.
  # TODO: Maybe there's a better way to handle permissions.

  configure.vm.synced_folder "development", "/srv/www", {
    :extra => "dmode=775,fmode=775",
    :group => "www-data",
    # :nfs   => true,
    :owner => "www-data",
  }

  if apache_or_nginx == "apache"
    configure.vm.synced_folder "shared/apache", "/etc/apache2", {
      :extra => "dmode=775,fmode=775",
      :group => "root",
      # :nfs   => true,
      :owner => "root",
    }
  end

  configure.vm.synced_folder "shared/mysql", "/etc/mysql", {
    :extra => "dmode=775,fmode=775",
    :group => "root",
    # :nfs   => true,
    :owner => "root",
  }

  if apache_or_nginx == "nginx"
    configure.vm.synced_folder "shared/nginx", "/etc/nginx", {
      :extra => "dmode=775,fmode=775",
      :group => "root",
      # :nfs   => true,
      :owner => "root",
    }
  end

  configure.vm.synced_folder "shared/php", "/etc/php5", {
    :extra => "dmode=775,fmode=775",
    :group => "root",
    # :nfs   => true,
    :owner => "root",
  }

  configure.vm.usable_port_range = 4096..8192
end
