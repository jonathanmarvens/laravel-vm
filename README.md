# Laravel VM.

## Setup.

### Install VirtualBox.

Head over to: [http://virtualbox.org](http://virtualbox.org "VirtualBox website."). Go to the downloads page and grab the appropriate package for system. Install it.

### Install Vagrant.

Head over to: [http://vagrantup.com](http://vagrantup.com "Vagrant website."). Go to the downloads page and grab the appropriate package for system. Install it.

## Clone this repo.

Go into your preferred directory and clone this repository (`jonathanmarvens/laravel-vm`).

## Usage.

Go into the directory where your clone lives and, from there, execute `vagrant up`. The last command *may* take a while, so go make yourself some coffee...you need it to keep you up for the awesomeness that you're gonna build with Laravel. After `vagrant up` has successfully (__hopefully__) completed, in your preferred web browser, navigate to `http://localhost:5272`. If all went well, you should see Laravel's start page.

Execute `vagrant ssh` to SSH into your running machine.

Configuration files for __apache__, __nginx__, __php__, & __mysql__ can be found at `shared/<apache|nginx|php|mysql>/` on the host. Also, all of the files for your Laravel application are located at `development/` on the host.

## License.

License you ask? What's this "*license*" thingy you talk about? Give it whatever effin' license you want...

## Notes.

- This document is a WIP and deemed *__incomplete__*.
- "laravel-vm" hasn't been tested on Windows. Let me know if your computer explodes.

---

__[:)]...enjoy!__
